module.exports = {
//////  comandosDeRegistro ///////
 nick : require("./Comandos/nick.js"),
  user : require("./Comandos/user.js"),
  password : require("./Comandos/password.js"),
  quit : require("./Comandos/quit.js"),
  usermod : require("./Comandos/usermode.js"),
//////  comandosDeRegistro ///////

/////////// operação de canal ///////////////
  join : require("./Comandos/join.js"),
  part : require("./Comandos/part.js"),
  channelmode : require("./Comandos/channelmode.js"),
  topic : require("./Comandos/topic.js"),
  names : require("./Comandos/names.js"),
  list : require("./Comandos/list.js"),
  invite : require("./Comandos/invite.js"),
  kick : require("./Comandos/kick.js"),
/////////// operação de canal ///////////////

/////////// ENVIO DE MENSAGENS /////////////
  notice : require("./Comandos/notice.js"),
  privmsg : require("./Comandos/privmsg.js"),
////////////////////////////////////////////

/////////////// CONSULTA A SERVIDOR///////////////////
  motd : require("./Comandos/motd.js"),
  lusers : require("./Comandos/lusers.js"),
  version : require("./Comandos/version.js"),
  stats : require("./Comandos/stats.js"),
  time : require("./Comandos/time.js"),
//////////////////////////////////////////////////////

///////////////// CONSULTA USUARIOS /////////////////


  whois : require("./Comandos/whois.js"),
  who : require("./Comandos/who.js"),
/////////////////////////////////////////////////////

////////////////// MISCELANEA ///////////////////
  kill : require("./Comandos/kill.js"),
  ping : require("./Comandos/ping.js"),
  pong : require("./Comandos/pong.js"),
/////////////////////////////////////////////////
};
