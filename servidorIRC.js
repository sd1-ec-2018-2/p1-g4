net = require('net');

net.createServer(function (socket) {
  socket.invisible = false;
  socket.restricted = false;
  socket.isOp = false;
  socket.registrado = false;

  global.clients.push(socket);

  do{
    socket.write("Por favor registre-se no servidor.\r\n")
    socket.on('data', function(data) {
    analisarRegistro(data, socket);
  });
  } while(socket.nick == '' || socket.user == '' );

  socket.write("Bem Vindo!\r\n");


  socket.on('end', function () {
    global.clients.splice(global.clients.indexOf(socket), 1);
  });

}).listen(5000);

console.log("Chat server running at port 5000\n");

global.SavedPass = {};
global.clients = [];
global.channels = {};

global.getCnames = function(){ 
   names = []; 
   for( name in global.channels )
      names.push( name ) ;
   return names;
};

global.broadCastSaida = function ( msg, socket){
	canais = global.getCnames();
	canais.forEach(function ( canal ){
		if( global.channels[ canal ]['users'].indexOf(socket) != -1 ){
			socks = global.channels[ canal ]['users'];
			socks.forEach( function (sock){
				if( sock != socket)
					sock.write(msg);
			});
		}
	
	});

};

global.sairCanais  = function (socket){
	canais = global.getCnames();
	for( i =0; i < canais.length; i++)
		if( (index=global.channels[ canais[ i ] ]['users'].indexOf(socket)) != -1 )
		global.channels[ canais[ i ] ]['users'].splice( index , 1 );
};

global.getCLientsNames = function (){
	names = [];
	global.clients.forEach( function (socket) {
		names.push( socket.nick );
	});
	return names;
};

var comandos = require("./comandos.js");

function analisarRegistro(data, socket) {
   var mensagem  = String(data).trim();
   var args = mensagem.split(" ");
   var comando = args[0].toUpperCase();
   args.splice(0, 1);
      switch( comando ){ 
		case 'NICK':
			comandos.nick.nick(args, socket);
		break;
		
		case 'USER':
			comandos.user.user(args, socket);
		break;
		case 'TIME':
			comandos.time.time(args, socket);
		break;
		case 'PASS':
			comandos.password.pass(args, socket);
		break;
		case 'QUIT':
			comandos.quit.quit(args, socket);
		break;
		case 'JOIN':
			comandos.join.join(args,socket);
		break;
		case 'PART':
			comandos.part.part(args, socket);
				
		break;
		case 'NAMES':
			comandos.names.names(args, socket);
		break;		
		case 'MOTD':
			comandos.motd.motd(args, socket);
		break;
		case 'PRIVMSG':
			comandos.privmsg.privmsg(args, socket);
		break;
		case  'LIST':
			comandos.list.list(args, socket);
		break;
		case 'TOPIC':
			comandos.topic.topic(args, socket);				
		break;
		case 'PING':
			comandos.ping.ping(args, socket);							
		break;
		case 'PONG':
			comandos.pong.pong(args,socket);
		break;
		case 'WHO':
			comandos.who.who(args, socket);
		break;
		case 'MODE':
			comandos.usermod.user(args,socket);
		break;
		default:
			socket.write("Digite um comando válido.\n");
		break;
	}

	
}
