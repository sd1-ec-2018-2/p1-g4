module.exports = {
names : function(args, socket) {
	if(socket.registrado == false)
				return socket.write(':localhost 451 * :Você não foi registrado ainda.\n');
   var i;
   if(args.length==0){
		canais = global.getCnames();
		canais.forEach( function ( canal ){
		   socket.write('Usuários ativos no canal "'+canal+'":\n=======================\n');
		   global.channels[ canal ]['users'].forEach( function ( usuario ){
					if( usuario.invisible == false)
						socket.write(usuario.nick+'\n');
				
		   });
		
		});

		return; 
   }

   cnames = [];
   if( args[0].indexOf(',')  == -1){
      cnames.push( args[0] );
   }
   else {
      cnames = args[0].split(',');
   }

   for( i = 0 ; i < cnames.length; i++ ){
      cname = cnames[i];
      if(global.channels[cname]){
         if( global.channels[cname]['users'].length > 0)
				socket.write('Usuários do canal: '+cname+'\n=======================\n');
            for(i=0; i<global.channels[cname]['users'].length; i++){
               if( global.channels[cname]['users'][i].invisible == false)
			socket.write(global.channels[cname]['users'][i].nick+'\n');
            
         }
      }
      else{
         socket.write(':localhost 403 '+cname+' NAMES :Não existe esse canal.\n');
      }
   }
}
};
