module.exports = {
part : function(args, socket) {
	if( args.length == 0)
		return socket.write(':localhost 461 '+socket.nick+' PART :Comando necessita de mais parâmetros.\n');
if(socket.registrado == false)
	return socket.write(':localhost 451 * :Você não foi registrado ainda.\n');

	canais = global.getCnames();
	sel = [];
	aux = args[0];
	if( aux.indexOf(',') == -1) // é um único canal
		sel.push(aux);
	else // caso seja uma lista de canais
		sel = aux.split(',');
	sel.forEach( function ( canal){
				if( !global.channels[ canal ] ) // canal não existe
				socket.write(':localhost 403 '+canal+' PART :O canal não existe.\n');	
				else{
						index = global.channels[ canal ] ['users'].indexOf(socket);
						if( index == -1) 
						socket.write(':localhost 442 '+canal+' PART :Você não está no canal.\n');	
					 else{
							global.channels[ canal ] ['users'].splice( index , 1 );
							global.channels[ canal ] ['users'].forEach( function ( usuario ){ // broadcast a saida do usuario para os outros membros do canal
							usuario.write(socket.username+'@localhost PART : saiu do canal \"'+canal+'\"\n');
							});
						socket.write(':localhost '+canal+' PART :Você foi removido com sucesso.\n');	
					 }
						
				}
});

}
};