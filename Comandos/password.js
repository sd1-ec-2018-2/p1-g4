module.exports = {
pass : function(args, socket) {
if(!args[0]) // usuário não definiu a senha
	return socket.write(':localhost 461 * PASS :A senha não foi definida.\n');

pass = args[0];
if( socket.registrado ) // verifica se o usuario já foi registrado
	return socket.write(':localhost 462 '+socket.nick+' :Você não pode registrar novamente.\n');
if( socket.nick || socket.username)
	return socket.write('ERRO: Comando deve ser executado antes dos comandos NICK/PASS.\n');

socket.password = pass;
}

};