module.exports = {
user : function(args, socket) {
	if(socket.registrado == false)
		return socket.write(':localhost 451 * :Você não foi registrado ainda.\n');
	if( args.length < 2 )
		return socket.write(':localhost 461 '+socket.nick+' MODE :Comando necessita de mais parâmetros.\n');
	if( args[0] != socket.nick)
		return socket.write(':localhost 461 '+socket.nick+' '+args[0]+' :Você não pode mudar o MODE de outros usuários.\n');

	mode = args[1].toLowerCase();
	usuario = args[0];
	switch( mode ){
		case '+i':
			socket.invisible = true;
			socket.write(':'+socket.nick+'!'+socket.username+'@localhost MODE '+socket.nick+' :+i\n');

		break;
		case '-i': 
			socket.invisible = false;
			socket.write(':'+socket.nick+'!'+socket.username+'@localhost MODE '+socket.nick+' :-i\n');
		break;
		case '+o':
			socket.write('ERRO: Você não pode garantir o titulo de operador a si mesmo.\n');
		break;
		case '-o':
			if(socket.isOp){
			socket.isOp = false;
			socket.write(':'+socket.nick+'!'+socket.username+'@localhost MODE '+socket.nick+' :-o\n');
			}
			else socket.write('ERRO: Você não é operador.\n');
		break;
		case '+r':
				socket.restricted = true;
					socket.write(':'+socket.nick+'!'+socket.username+'@localhost MODE '+socket.nick+' :+r\n');
		break;
		case '-r':
				socket.restricted = false;
					socket.write(':'+socket.nick+'!'+socket.username+'@localhost MODE '+socket.nick+' :-r\n');
		break;
		default:
			 return socket.write('ERRO: Comando MODE com parâmetros inválidos.\n');
		break;
	}
}
};
