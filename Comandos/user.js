module.exports = {
user : function(args, socket) {
if(args.length < 4)
	return socket.write(':localhost 461 * USER :Número insuficiente de parâmetros\n');
user = args[0];
mode = Number(args[1]);

if( isNaN( mode ))
	return socket.write('ERRO: parâmetro MODE inválido.\n');
 
if( /\W/g.test(user) ) 
		return socket.write(':localhost 432 * '+user+' :Caracteres especiais não são permitidos no username.\n');
if( socket.registrado )
		return socket.write(':localhost 433 * '+socket.username+' :Você já foi registrado.\n');

for( i = 0; i < global.clients.length; i++){ 
	s = global.clients[i];
	if( s.username == user)
		return socket.write(':localhost 433 * '+user+' :Username já está em uso.\n');
}

fullname = args.splice(3, args.length).join(" ");
if(fullname[0] == ':')
	fullname = fullname.substr(1,fullname.length);
socket.fullname = fullname;
socket.username = user;
socket.mode = mode;
if( socket.nick ){
	socket.registrado = true;
		if( socket.password)
				global.SavedPass[nick] = socket.password;
	socket.write('Registrado com sucesso.\n');
}
}

};
