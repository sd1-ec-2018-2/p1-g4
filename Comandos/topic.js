module.exports = {
topic : function(args, socket) {
	if(socket.registrado == false)
		return socket.write(':localhost 451 * :Você não foi registrado ainda.\n');	
	if( args.length == 0 )	
		socket.write(':localhost 461 '+socket.nick+' TOPIC :Comando necessita de mais parâmetros.\n');
	if(!global.channels[ canal ])
		return socket.write(':localhost 403 '+canal+' TOPIC :O canal não existe.\n');
	canal = args[0];
	if( global.channels[ canal ]['users'].indexOf(socket) == -1 )
		return socket.write('Você não está no canal "'+canal+'".\n');
	if( args.length == 1){
		if((topic=global.channels[ canal].topic) == '')
			return socket.write(':localhost 331 '+canal+' :Topic não foi definido.\n');
		socket.write(':localhost 332 '+canal+' :'+topic+'\n');
		
	}
	if( args.length > 1){
		args.splice(0,1);
		topic = args.join(' ');
		if( topic.length == 1)
			topic = ''; 		
		if(topic[0] == ':')
			topic = topic.substr(1,topic.length);	
		global.channels[ canal ].topic = topic;
		if( topic == '')
			return socket.write('TOPIC resetado com sucesso.\n');
		socket.write('TOPIC definido com sucesso.\n');
	}
	
}
};
