module.exports = {
privmsg : function(args, socket) {
	if(socket.registrado == false)
		return socket.write(':localhost 451 * :Você não foi registrado ainda.\n');
	if( args.length <= 1 )
	return socket.write(':localhost 461 '+socket.nick+' PRIVMSG :Comando necessita de mais parâmetros.\n');
	
	target = args[0];
	args.splice(0,1);
	msg = args.join(' ');
	if(msg[0] == ':')
		msg = msg.substr(1, msg.length);
	else 
		msg = args[0];
	users = [];
		global.clients.forEach( function (socket) {
			users.push( socket.nick );
	});
	if( target[0].match(/\&|\#|\+|\!/) ) { // caso seja um canal
		if( !global.channels[ target ] )
			return socket.write('ERRO: O canal '+target+' não existe.\n');
		if( global.channels[ target ]['users'].indexOf(socket) == -1)
			return socket.write('ERRO: Você não é membro do canal.\n');

		global.channels[ target ]['users'].forEach( function ( sock ){
				if( sock != socket)
					sock.write(':localhost '+target+' '+socket.nick+' PRIVMSG: '+msg+'\n');
		});
		return;	
	}
	if( users.indexOf( target ) == -1) 
		return socket.write('ERRO: O usuário '+target+' não existe.\n');
	if( global.clients [ users.indexOf( target ) ].restricted == false) 
		global.clients [ users.indexOf( target ) ].write( ':localhost '+socket.nick+' PRIVMSG: '+msg+'\n' );
	else{
		if( !socket.password )  // não é um usuario registrado com senha
			return socket.write('ERRO: Você não pode mandar mensagem para esse usuário.\n');
		global.clients [ users.indexOf( target ) ].write( ':localhost '+socket.nick+' PRIVMSG: '+msg+'\n' );		
	}
}	
};
