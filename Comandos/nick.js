module.exports = {

nick : function(args, socket) {
	
if(!args[0])   // checa se o nick foi informado ERR_NONICKNAMEGIVEN
		return socket.write(':localhost 431 * :Nenhum nick foi informado.\n');

nick = args[0];
if( /\W/g.test(nick) ) // checa o nick contem caracteres especiais ERR_ERRONEUSNICKNAME
	return socket.write(':localhost 432 * '+nick+' :Caracteres especiais não são permitidos no nickname.\n');
if( nick.length > 9 ){
	return socket.write(':localhost 432 * '+nick+' :Nick com tamanho maior do que o máximo permitido(9 chars).\n');
}

for( i = 0; i < global.clients.length; i++){ // ERR_NICKNAMEINUSE
	s = global.clients[i];
	if( s.nick == nick && s == socket)
		return socket.write(':localhost 433 * '+nick+' :Você já está usando esse nickname.\n');
	if( s.nick == nick)
		return socket.write(':localhost 433 * '+nick+' :Nickname já está em uso.\n');
};
if( global.SavedPass[nick] ){
	if(!socket.password)
		return socket.write('ERRO: NICK com senha definida. Favor executar comando PASS.\n');
	senha = global.SavedPass[nick]; // senha salva
	if( senha != socket.password)
			return socket.write('ERRO: Senha não confere\n');
		
}
socket.nick = nick;
if( socket.registrado )
	socket.write('Nickname alterado com sucesso.\n');

if( socket.username) {
	if(socket.registrado == false)
		socket.write('Registrado com sucesso.\n');
	socket.registrado = true;;
	if( socket.password)
				global.SavedPass[nick] = socket.password;
}
}


};
