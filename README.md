# PROJETO 1 - GRUPO 4 SD1

## MEMBROS
* Lucas Rodrigues Porto - lucasrodriguesporto@gmail.com - líder
* Cássio Gomes de Souza - netfuzzerr@gmail.com - Desenvolvedor
* Matheus Augusto Monteiro de Godoy - matheusamg@gmail.com - Desenvolvedor
* Rheion Logan Nobre Dias - rheiondias@outlook.com - Desenvolvedor

# COMANDOS IMPLEMENTADOS ATÉ AGORA
* USER
* PASS
* NICK
* QUIT
* TIME
* JOIN
* PART
* NAMES
* MOTD
* PRIVMSG
* LIST
* TOPIC
* PING
* PONG
* WHO
* MODE 

# TO-DO LIST
* CHANNELMODE
* INVITE
* KICK
* KILL
* LUSERS
* NOTICE
* STATS
* VERSION
* WHOIS
